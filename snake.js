	window.addEventListener("load", () => {
	    canvas = document.querySelector("canvas");
	    ctx = canvas.getContext("2d");
	    window.addEventListener("keydown", pulsar);
	    setInterval(juego, 20);
	    xSnake = 100;
	    ySnake = 100;
	    direccion = "drcha";
	    xComida = Math.floor((Math.random() * canvas.width) + 1);
	    yComida = Math.floor((Math.random() * canvas.height) + 1);
	    console.log(xComida, yComida);
	    puntos = 1;
	    cola = [];

	});

	function pulsar(event) {
	    console.log(event.keyCode);
	    switch (event.keyCode) {
	        case 37:
	            direccion = "izqda";
	            break;
	        case 38:
	            direccion = "arriba";
	            break;
	        case 39:
	            direccion = "drcha";
	            break;
	        case 40:
	            direccion = "abajo";
	            break;
	    }
	    console.log(cola);
	}

	function juego() {
	    switch (direccion) {
	        case "izqda":
	            xSnake--;
	            break;
	        case "arriba":
	            ySnake--;
	            break;
	        case "drcha":
	            xSnake++;
	            break;
	        case "abajo":
	            ySnake++;
	            break;
	    }
	    //pintar lienzo cada intervalo de tiempo
	    ctx.fillStyle = "lightblue";
	    ctx.fillRect(0, 0, canvas.width, canvas.height);

	    //pintar comida 

	    ctx.fillStyle = "tomato";
	    ctx.fillRect(xComida, yComida, 5, 5);

	    //pintar serpiente a cada intervalo de tiempo
	    ctx.fillStyle = "black";

	    /*si la posicion X de la serpiente es mayor al ancho del marco (tope drcha) que se ponga en posicion 0 (tope a la izqda)*/
	    if (xSnake > canvas.width) {
	        xSnake = 0;
	    }
	    /*si la posicion X de la serpiente es menor a 0 (tope izqda) que se ponga en el tope del marco 
	    (tope a la drcha)*/
	    if (xSnake < 0) {
	        xSnake = canvas.width;
	    }
	    /*Si la posicion Y de la serpiente es mayor al alto del marco, se pondrá a 0 (tope arriba)*/
	    if (ySnake > canvas.height) {
	        ySnake = 0;
	    }
	    /*Si la posicion Y de la serpiente es menor a 0, aparecerá por el otro lado (tope  abajo)*/
	    if (ySnake < 0) {
	        ySnake = canvas.width;
	    }

	    cola.push({ x: xSnake, y: ySnake });
	    while (cola.length > puntos) {
	        cola.shift();
	    }

	    ctx.fillRect(xSnake, ySnake, 5, 5);
	    for (i = 0; i < puntos; i++) {
	        ctx.fillRect(cola[i].x, cola[i].y, 5, 5);
	    }



	    //si coind¡ciden las posiciones de la serpiente y la comida, se suma un punto
	    if (xSnake == xComida && ySnake == yComida) {
	        puntos++;
	        document.querySelector(".puntos").innerHTML = "puntos: " + puntos;
	        console.log("Comido!", puntos);

	    }


	}